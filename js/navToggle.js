let toggleBtn = true;
document.getElementById("btnControl").onclick = function showNav() {
  if (toggleBtn == true) {
    document.body.style.gridTemplateAreas =
      '"header header header" "nav article article" "footer footer footer"';
    document.getElementById("mainNav").style = "display: block";
    toggleBtn = false;
  } else {
    document.body.style.gridTemplateAreas =
      '"header header header" "article article article" "footer footer footer"';
    document.getElementById("mainNav").style = "display: none";
    toggleBtn = true;
  }
};
